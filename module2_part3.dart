//@Author: Mogamad Tayyib Samsodien
class MTNAppWinner {
  String appname = '';
  String category = '';
  String dev = '';
  String year = '';

  MTNAppWinner(String name, String category, String dev, String year) {
    this.appname = name;
    this.category = category;
    this.dev = dev;
    this.year = year;
  }
  void caps() {
    print(this.appname.toUpperCase());
  }
}

void main() {
  var Ambani = MTNAppWinner("Ambani", "Education", "Mukundi Lambani", "2021");
  print("Name of App: " +
      Ambani.appname +
      "\nCategory: " +
      Ambani.category +
      "\nDeveloper: " +
      Ambani.dev +
      "\nYear won: " +
      Ambani.year);
  Ambani.caps();
}
